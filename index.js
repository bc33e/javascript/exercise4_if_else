// BÀI 1
function sapXepThuTu() {
  var num1 = Number(document.getElementById("so-thu-nhat").value);
  var num2 = Number(document.getElementById("so-thu-hai").value);
  var num3 = Number(document.getElementById("so-thu-ba").value);
  console.log({ num1, num2, num3 });
  var sapXep = 0;
  if (num1 > num2 && num2 > num3) {
    sapXep = num3 < num2 < num1;
    document.getElementById(
      "dem-so-thu-tu"
    ).innerHTML = `Kết quả: ${num3} < ${num2} < ${num1}`;
  } else if (num1 > num2 && num1 > num3) {
    sapXep = num2 < num3 < num1;
    document.getElementById(
      "dem-so-thu-tu"
    ).innerHTML = `Kết quả: ${num2} < ${num3} < ${num1}`;
  } else if (num1 > num2 && num3 > num1) {
    sapXep = num2 < num1 < num3;
    document.getElementById(
      "dem-so-thu-tu"
    ).innerHTML = `Kết quả: ${num2} < ${num1} < ${num3}`;
  } else if (num2 > num1 && num1 > num3) {
    sapXep = num3 < num1 < num2;
    document.getElementById(
      "dem-so-thu-tu"
    ).innerHTML = `Kết quả: ${num3} < ${num1} < ${num2}`;
  } else if (num3 > num1 && num2 > num3) {
    sapXep = num1 < num3 < num2;
    document.getElementById(
      "dem-so-thu-tu"
    ).innerHTML = `Kết quả: ${num1} < ${num3} < ${num2}`;
  } else {
    sapXep = num1 < num2 < num3;
    document.getElementById(
      "dem-so-thu-tu"
    ).innerHTML = `Kết quả: ${num1} < ${num2} < ${num3}`;
  }
  //   console.log("Thứ tự: ", sapXep);
}

// BÀI 2

document.getElementById("gui-loi-chao").onclick = function () {
  var nameChange = document.getElementById("danh-sach-ten").value;
  console.log("Name: ", nameChange);
  var loiNhan = document.getElementById("loi-nhan");
  if (nameChange === "") {
    loiNhan.innerHTML = "Bello";
  } else if (nameChange === "gru") {
    loiNhan.innerHTML = "Bello Mini Boss";
  } else if (nameChange === "bob") {
    loiNhan.innerHTML = "Bello King Bob";
  } else if (nameChange === "kevin") {
    loiNhan.innerHTML = "Bello Kevin";
  } else if (nameChange === "stuart") {
    loiNhan.innerHTML = "Bello Stuart";
  }
};

// BÀI 3
document.getElementById("dem-so").onclick = function () {
  var soThuNhat = Number(document.getElementById("num-1").value);
  var soThuHai = Number(document.getElementById("num-2").value);
  var soThuBa = Number(document.getElementById("num-3").value);
  console.log({ soThuNhat, soThuHai, soThuBa });
  var ketQuaSoDem = document.getElementById("ket-qua-3");
  var soChan = 0;
  var soLe = 0;

  if (soThuNhat % 2 == 0) {
    soChan += 1;
  }
  if (soThuHai % 2 == 0) {
    soChan += 1;
  }
  if (soThuBa % 2 == 0) {
    soChan += 1;
  }
  soLe = 3 - soChan;
  console.log("Số Lẻ: ", soLe);
  console.log("Số Chẵn: ", soChan);
  ketQuaSoDem.innerHTML = `Có ${soChan} số chẵn và ${soLe} số lẻ`;
};

// BÀI 4
document.getElementById("du-doan").onclick = function () {
  var canh1 = document.getElementById("canh-thu-nhat").value * 1;
  var canh2 = document.getElementById("canh-thu-hai").value * 1;
  var canh3 = document.getElementById("canh-thu-ba").value * 1;
  console.log({ canh1, canh2, canh3 });
  var ketQua = document.getElementById("ket-qua-4");
  if (canh1 == canh2 && canh2 == canh3) {
    ketQua.innerHTML = "Đây là tam giác đều";
  } else if (canh1 == canh2 || canh3 == canh1 || canh2 == canh3) {
    ketQua.innerHTML = "Đây là tam giác cân";
  } else if (
    canh1 * canh1 == canh2 * canh2 + canh3 * canh3 ||
    canh2 * canh2 == canh1 * canh1 + canh3 * canh3 ||
    canh3 * canh3 == canh2 * canh2 + canh1 * canh1
  ) {
    ketQua.innerHTML = "Đây là tam giác vuông";
  } else {
    ketQua.innerHTML = "Đây là tam giác nào đó";
  }
};
